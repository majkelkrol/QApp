import Foundation


struct Answer: Identifiable {
    var id = UUID()
    var text: String
    var isCorrect: Bool
}
