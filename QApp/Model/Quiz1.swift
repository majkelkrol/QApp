import Foundation
import SwiftUI
import UIKit


struct Quiz1: Codable, Hashable, Equatable {
    let celebrity: Celebrity
    let opinionsEnabled: Bool
    let rates: [Rate]
    let questions: [Question]
    let createdAt: Date
    let sponsored: Bool
    let title, type, content: String
    let tags: [CategoryElement]
    let buttonStart, shareTitle: String
    let categories: [CategoryElement]
    let id: Int
    let scripts: String
    let mainPhoto: MainPhoto
    let category: PurpleCategory
    let isBattle: Bool
    let created: Int
    let canonical, productURL: String
    let publishedAt: Date
    let latestResults: [LatestResult]
    let avgResult: Double
    let resultCount: Int
    let cityAvg, cityTime, cityCount: JSONNull?
    let userBattleDone: Bool
    let sponsoredResults: SponsoredResults

    enum CodingKeys: String, CodingKey {
        case celebrity
        case opinionsEnabled = "opinions_enabled"
        case rates, questions, createdAt, sponsored, title, type, content, tags, buttonStart, shareTitle, categories, id, scripts, mainPhoto, category, isBattle, created, canonical
        case productURL = "productUrl"
        case publishedAt, latestResults, avgResult, resultCount, cityAvg, cityTime, cityCount, userBattleDone, sponsoredResults
    }
    
}

struct CategoryElement: Codable, Hashable {
    let uid: Int
    let secondaryCid: String?
    let name, type: String
    let primary: Bool?
}

struct PurpleCategory: Codable {
    let id: Int
    let name: String
}

struct Celebrity: Codable, Hashable {
    let result, imageAuthor, imageHeight, imageURL: String
    let show: Int
    let name, imageTitle, imageWidth, content: String
    let imageSource: String

    enum CodingKeys: String, CodingKey {
        case result, imageAuthor, imageHeight
        case imageURL = "imageUrl"
        case show, name, imageTitle, imageWidth, content, imageSource
    }
}

struct LatestResult: Codable {
    let city: Int
    let endDate: String
    let result: Double
    let resolveTime: Int

    enum CodingKeys: String, CodingKey {
        case city
        case endDate = "end_date"
        case result, resolveTime
    }
}

struct MainPhoto: Codable, Hashable {
    let author: String
    let width: Int
    let source, title: String
    let url: String
    let height: Int
}

struct Question: Codable, Hashable {
    let image: Image
    let answers: [AnswerElement]
    let text: String
    let answer: AnswerEnum
    let type: TypeEnum
    let order: Int
}

enum AnswerEnum: String, Codable {
    case answerText = "ANSWER_TEXT"
}

struct AnswerElement: Codable {
    let image: Image
    let order: Int
    let text: String
    let isCorrect: Int?
}

struct Image: Codable {
    let author, width, mediaID, source: String
    let url, height: String

    enum CodingKeys: String, CodingKey {
        case author, width
        case mediaID = "mediaId"
        case source, url, height
    }
}

enum TypeEnum: String, Codable {
    case questionText = "QUESTION_TEXT"
}

struct Rate: Codable, Hashable  {
    let from, to: Int
    let content: String
}

struct SponsoredResults: Codable {
    let imageAuthor, imageHeight, imageURL, imageWidth: String
    let textColor, content, mainColor, imageSource: String

    enum CodingKeys: String, CodingKey {
        case imageAuthor, imageHeight
        case imageURL = "imageUrl"
        case imageWidth, textColor, content, mainColor, imageSource
    }
}


class JSONNull: Codable, Hashable {

    public static func == (lhs: JSONNull, rhs: JSONNull) -> Bool {
        return true
    }

    public var hashValue: Int {
        return 0
    }

    public init() {}

    public required init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if !container.decodeNil() {
            throw DecodingError.typeMismatch(JSONNull.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for JSONNull"))
        }
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encodeNil()
    }
}

extension Bundle {
    func decode<T: Decodable>(_ type: T.Type, from file: String) -> T {
        guard let url = self.url(forResource: file, withExtension: nil) else {
            fatalError("Failed to locate \(file) in bundle")
        }
        
        guard let data = try? Data(contentsOf: url) else {
            fatalError("Failed to load \(file) from bundle")
        }
        
        let decoder = JSONDecoder()
        
        guard let loaded = try? decoder.decode(T.self, from: data) else {
            fatalError("Failed to decode \(file) from bundle")
        }
        return loaded
    }
    
}
