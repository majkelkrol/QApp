import SwiftUI


struct WebView: View {
    var body: some View {
        VStack(spacing: 100) {
            HStack(spacing: 50) {
                NavigationLink {
                    ContentWebView1()
                } label: {
                    Image("tablice")
                        .resizable()
                        .frame(width: 150, height: 150, alignment: .center)

                }
                
                NavigationLink {
                    ContentWebView2()
                } label: {
                    Image("zwierzaki")
                        .resizable()
                        .frame(width: 150, height: 150, alignment: .center)
                }
            }
            
            HStack(spacing: 50) {
                NavigationLink {
                    ContentWebView3()
                } label: {
                    Image("mozg")
                        .resizable()
                        .frame(width: 150, height: 150, alignment: .center)
                }
                
                NavigationLink {
                    ContentWebView4()
                } label: {
                    Image("naukowcy")
                        .resizable()
                        .frame(width: 150, height: 150, alignment: .center)
                }
            }
            
            HStack(spacing: 50) {
                NavigationLink {
                    ContentWebView5()
                } label: {
                    Image("awaria")
                        .resizable()
                        .frame(width: 150, height: 150, alignment: .center)
                }
                

                NavigationLink {
                    ContentWebView6()
                } label: {
                    Image("word")
                        .resizable()
                        .frame(width: 150, height: 150, alignment: .center)

                }
            }
        }
        .navigationTitle("Wybierz swój quiz!")
    }
}

struct WebView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            WebView()
        }
    }
}
