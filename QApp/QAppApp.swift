
import SwiftUI

@main
struct QAppApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
