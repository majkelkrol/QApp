import SwiftUI


struct QuizView: View {
    let quiz1 = Bundle.main.decode([Quiz1].self, from: "Quiz1.json")
    //let quiz: Quiz1
    
    var body: some View {
        VStack(spacing: 30) {
            HStack {
                Text("Quiz dla fanów motoryzacji. Rozpoznasz te tablice rejestracyjne?")
                    .font(.body)
                
                Spacer()
                
                Text("1 z 12")
                    .font(.title)
            }
            .padding()
            
            BarView()
            
            VStack(spacing: 20) {
                List(quiz1, id: \.self) { item in
                    Text(item.title)
                }
                //Text(quiz1.title)
                //Text(quiz.title)
                
                //AnswerRow(answer: Answer(text: "Gdańsk", isCorrect: false))
                //AnswerRow(answer: Answer(text: "Gdynia", isCorrect: true))
                //AnswerRow(answer: Answer(text: "Gocław", isCorrect: false))
            }
            
            Button {
                //
            } label: {
                Text(">>>")
            }
            Spacer()
        }
    }
}

struct QuizView_Previews: PreviewProvider {
    static var previews: some View {
        QuizView()
        //QuizView(quiz: Quiz1)
    }
}

